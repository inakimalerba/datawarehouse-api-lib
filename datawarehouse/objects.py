"""API Objects."""
from datetime import datetime
from dateutil import parser
from dataclasses_json import config
from typing import List, Optional
from marshmallow import fields

from restclient.base import (
    RESTManager, RESTObject, GETMethod, LISTMethod, CREATEMethod, DELETEMethod, ObjectDELETEMethod,
)


class Maintainer(RESTObject):
    pass


class File(RESTObject):
    pass


class TestStat(RESTObject):
    pass


class TestStatManager(RESTManager, GETMethod):
    _obj_cls = TestStat
    _path = 'api/1/test/{test_id}/stats'
    _from_parent_attrs = ('test_id:id', )


class Test(RESTObject):
    _managers = {
        'stats': 'TestStatManager',
        'issuerecords': 'TestIssueRecordManager'
    }


class TestManager(RESTManager, GETMethod, LISTMethod):
    _obj_cls = Test
    _path = 'api/1/test'
    _listed_by = 'tests'


class TestRun(RESTObject):
    pass


class TestRunManager(RESTManager, LISTMethod):
    _obj_cls = TestRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/test'
    _listed_by = 'jobs'
    _from_parent_attrs = ('pipeline_id', )


class Patch(RESTObject):
    pass


class PatchManager(RESTManager, LISTMethod):
    _obj_cls = Patch
    _path = 'api/1/pipeline/{pipeline_id}/patches'
    _listed_by = 'patches'
    _from_parent_attrs = ('pipeline_id', )


class Report(RESTObject):
    pass


class ReportManager(RESTManager, LISTMethod, CREATEMethod):
    _obj_cls = Report

    _path = 'api/1/pipeline/{pipeline_id}/report'
    _listed_by = 'reports'
    _from_parent_attrs = ('pipeline_id', )


class IssueRecord(RESTObject, ObjectDELETEMethod):
    pass


class IssueRecordManager(RESTManager, LISTMethod):
    _obj_cls = IssueRecord

    _path = 'api/1/issue/{issue_id}/record'
    _listed_by = 'records'
    _from_parent_attrs = ('issue_id:id', )


class PipelineIssueRecordManager(RESTManager, GETMethod, LISTMethod,
                                 CREATEMethod, DELETEMethod):
    _obj_cls = IssueRecord
    _path = 'api/1/pipeline/{pipeline_id}/issue/record'
    _listed_by = 'issue_records'
    _from_parent_attrs = ('pipeline_id', )


class TestIssueRecordManager(RESTManager, LISTMethod):
    _obj_cls = IssueRecord
    _path = 'api/1/test/{test_id}/issue/record'
    _listed_by = 'issues'
    _from_parent_attrs = ('test_id:id', )


class Action(RESTObject):
    pass


class ActionManager(RESTManager, LISTMethod, CREATEMethod):
    _obj_cls = Action

    _path = 'api/1/pipeline/{pipeline_id}/action'
    _listed_by = 'actions'
    _from_parent_attrs = ('pipeline_id', )


class Issue(RESTObject):
    _managers = {
        'record': 'IssueRecordManager'
    }


class IssueManager(RESTManager, GETMethod, LISTMethod):
    _obj_cls = Issue

    _path = 'api/1/issue'
    _listed_by = 'issues'


class LintRun(RESTObject):
    pass


class LintRunManager(RESTManager, LISTMethod):
    _obj_cls = LintRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/lint'
    _listed_by = 'jobs'
    _from_parent_attrs = ('pipeline_id', )


class MergeRun(RESTObject):
    pass


class MergeRunManager(RESTManager, LISTMethod):
    _obj_cls = MergeRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/merge'
    _listed_by = 'jobs'
    _from_parent_attrs = ('pipeline_id', )


class BuildRun(RESTObject):
    pass


class BuildRunManager(RESTManager, LISTMethod):
    _obj_cls = BuildRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/build'
    _listed_by = 'jobs'
    _from_parent_attrs = ('pipeline_id', )


class Pipeline(RESTObject):
    _id = 'pipeline_id'
    _managers = {
        'lintruns': 'LintRunManager',
        'mergeruns': 'MergeRunManager',
        'buildruns': 'BuildRunManager',
        'testruns': 'TestRunManager',
        'patches': 'PatchManager',
        'reports': 'ReportManager',
        'issuerecords': 'PipelineIssueRecordManager',
        'actions': 'ActionManager',
        'failures': 'PipelineFailuresManager',
    }


class PipelineManager(RESTManager, GETMethod):
    _obj_cls = Pipeline
    _path = 'api/1/pipeline'


class PipelineFailedJobManager(RESTManager, LISTMethod):
    _obj_cls = Pipeline
    _listed_by = 'jobs'
    _from_parent_attrs = ('pipeline_id', )


class PipelineFailedLintManager(PipelineFailedJobManager):
    _obj_cls = LintRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/lint/failures'


class PipelineFailedMergeManager(PipelineFailedJobManager):
    _obj_cls = MergeRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/merge/failures'


class PipelineFailedBuildManager(PipelineFailedJobManager):
    _obj_cls = BuildRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/build/failures'


class PipelineFailedTestManager(PipelineFailedJobManager):
    _obj_cls = TestRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/test/failures'


class PipelineFailuresManager(RESTManager):
    _managers = {
        'lint': 'PipelineFailedLintManager',
        'merge': 'PipelineFailedMergeManager',
        'build': 'PipelineFailedBuildManager',
        'test': 'PipelineFailedTestManager',
    }
    _from_parent_attrs = ('pipeline_id', )


class PipelineMissingReportManager(RESTManager, LISTMethod):
    _obj_cls = Pipeline
    # This needs more hacking.
    _listed_by = 'pipelines'
    _path = 'api/1/report/missing'


class PipelineFailedManager(RESTManager, LISTMethod):
    _obj_cls = Pipeline
    # This needs more hacking.
    _listed_by = 'pipelines'
    _path = 'api/1/pipeline/failures/{stage}'


class IssueRegex(RESTObject):
    pass


class IssueRegexManager(RESTManager, LISTMethod):
    _obj_cls = IssueRegex
    _path = 'api/1/issue/regex'
    _listed_by = 'issue_regexes'


class KCIDBData(RESTObject):
    pass


class KCIDBEndpointManager(RESTManager, GETMethod):
    _obj_cls = KCIDBData
    _path = 'api/1/kcidb/data/{name}'


class KCIDBRevision(RESTObject):
    _managers = {
        'builds': 'KCIDBRevisionBuildManager',
        'issues': 'KCIDBRevisionIssueManager',
    }


class KCIDBRevisionManager(RESTManager, GETMethod, LISTMethod):
    _obj_cls = KCIDBRevision
    _path = 'api/1/kcidb/revisions'


class KCIDBBuild(RESTObject):
    _managers = {
        'tests': 'KCIDBBuildTestManager',
        'issues': 'KCIDBBuildIssueManager',
    }


class KCIDBBuildManager(RESTManager, GETMethod):
    _obj_cls = KCIDBBuild
    _path = 'api/1/kcidb/builds'


class KCIDBRevisionBuildManager(RESTManager, LISTMethod):
    _obj_cls = KCIDBBuild
    _from_parent_attrs = ('revision_iid:misc.iid',)
    _path = 'api/1/kcidb/revisions/{revision_iid}/builds'


class KCIDBTest(RESTObject):
    _managers = {
        'issues': 'KCIDBTestIssueManager',
    }


class KCIDBTestManager(RESTManager, GETMethod):
    _obj_cls = KCIDBTest
    _path = 'api/1/kcidb/tests'


class KCIDBBuildTestManager(RESTManager, LISTMethod):
    _obj_cls = KCIDBTest
    _from_parent_attrs = ('build_iid:misc.iid',)
    _path = 'api/1/kcidb/builds/{build_iid}/tests'


class KCIDBSubmitManager(RESTManager, CREATEMethod):
    _obj_cls = KCIDBData
    _path = 'api/1/kcidb/submit'


class KCIDBRevisionIssueManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    _obj_cls = Issue
    _from_parent_attrs = ('revision_iid:misc.iid',)
    _path = 'api/1/kcidb/revisions/{revision_iid}/issues'


class KCIDBBuildIssueManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    _obj_cls = Issue
    _from_parent_attrs = ('build_iid:misc.iid',)
    _path = 'api/1/kcidb/builds/{build_iid}/issues'


class KCIDBTestIssueManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    _obj_cls = Issue
    _from_parent_attrs = ('test_iid:misc.iid',)
    _path = 'api/1/kcidb/tests/{test_iid}/issues'
