"""Datawarehouse Client."""
from restclient import base

from datawarehouse import objects


class KCIDB:
    def __init__(self, api):
        self.data = objects.KCIDBEndpointManager(api)
        self.revisions = objects.KCIDBRevisionManager(api)
        self.builds = objects.KCIDBBuildManager(api)
        self.tests = objects.KCIDBTestManager(api)
        self.submit = objects.KCIDBSubmitManager(api)


class Datawarehouse:
    # pylint: disable=too-few-public-methods
    """Datawarehouse client."""

    def __init__(self, host, token=None):
        api = base.APIManager(host, token)
        self.pipeline = objects.PipelineManager(api)
        self.failed_pipelines = objects.PipelineFailedManager(api)
        self.pipelines_missing_report = objects.PipelineMissingReportManager(api)
        self.issue = objects.IssueManager(api)
        self.issue_regex = objects.IssueRegexManager(api)
        self.test = objects.TestManager(api)
        self.kcidb = KCIDB(api)
