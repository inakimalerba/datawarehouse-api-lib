import unittest
import responses

import datawarehouse


DW_API = 'http://server/api/1'


class TestObjects(unittest.TestCase):
    # pylint: disable=no-member
    """Test requests are performed correctly."""

    def setUp(self):
        self.dw = datawarehouse.Datawarehouse('http://server', 'my-secret-token')
        self.mock_responses()

    @staticmethod
    def mock_responses():
        """Mock all API calls."""
        def generic_list(grouped_by=None, id_key='id'):
            """Get a generic list of json objects."""
            g_list = [{id_key: 1}, {id_key: 2}]
            if grouped_by:
                g_list = {grouped_by: g_list}
            return {'results': g_list}

        get_endpoints = [
            ('/pipeline/123', {'pipeline_id': 123}),
            ('/pipeline/123/report', generic_list('reports')),
            ('/pipeline/123/action', generic_list('actions')),
            ('/pipeline/123/patches', generic_list('patches')),
            ('/pipeline/123/issue/record', generic_list('issue_records')),
            ('/pipeline/123/jobs/lint', generic_list('jobs')),
            ('/pipeline/123/jobs/lint/failures', generic_list('jobs')),
            ('/pipeline/123/jobs/merge', generic_list('jobs')),
            ('/pipeline/123/jobs/merge/failures', generic_list('jobs')),
            ('/pipeline/123/jobs/build', generic_list('jobs')),
            ('/pipeline/123/jobs/build/failures', generic_list('jobs')),
            ('/pipeline/123/jobs/test', generic_list('jobs')),
            ('/pipeline/123/jobs/test/failures', generic_list('jobs')),
            ('/issue', generic_list('issues')),
            ('/issue/1', {'id': 1}),
            ('/issue/1/record', generic_list('records')),
            ('/issue/regex', generic_list('issue_regexes')),
            ('/report/missing', generic_list('pipelines', 'pipeline_id')),
            ('/test', generic_list('tests')),
            ('/test/2', {'id': 2}),
            ('/test/2/stats', {'id': 2}),
            ('/kcidb/data/revisions?'
             'name=revisions&last_retrieved_id=0&limit=1', {'id': 1}),
            ('/kcidb/data/builds?'
             'name=builds&last_retrieved_id=0&limit=1', {'id': 1}),
            ('/kcidb/data/tests?'
             'name=tests&last_retrieved_id=0&limit=1', {'id': 1}),
            ('/pipeline/failures/lint?stage=lint', generic_list('pipelines', 'pipeline_id')),
            ('/pipeline/failures/merge?stage=merge', generic_list('pipelines', 'pipeline_id')),
            ('/pipeline/failures/build?stage=build', generic_list('pipelines', 'pipeline_id')),
            ('/pipeline/failures/test?stage=test', generic_list('pipelines', 'pipeline_id')),
            ('/kcidb/revisions', {'results': [{'id':1, 'misc': {'iid': 1}}]}),
            ('/kcidb/revisions/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/revisions/1/builds', {'results': [{'id':1, 'misc': {'iid': 1}}]}),
            ('/kcidb/revisions/1/issues', generic_list()),
            ('/kcidb/revisions/1/issues/1', {'id': 1}),
            ('/kcidb/builds/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/builds/1/tests', {'results': [{'id':1, 'misc': {'iid': 1}}]}),
            ('/kcidb/builds/1/issues', generic_list()),
            ('/kcidb/builds/1/issues/1', {'id': 1}),
            ('/kcidb/tests/1', {'id': 1, 'misc': {'iid': 1}}),
            ('/kcidb/tests/1/issues', generic_list()),
            ('/kcidb/tests/1/issues/1', {'id': 1}),
        ]

        for endpoint, payload in get_endpoints:
            responses.add(responses.GET, DW_API + endpoint, json=payload)

        post_endpoints = [
            ('/kcidb/submit', {}),
            ('/kcidb/revisions/1/issues', {'id': 2}),
            ('/kcidb/builds/1/issues', {'id': 2}),
            ('/kcidb/tests/1/issues', {'id': 2}),
        ]

        for endpoint, payload in post_endpoints:
            responses.add(responses.POST, DW_API + endpoint, json=payload)


    @responses.activate
    def test_get_pipeline(self):
        """Test get pipeline."""

        pipeline = self.dw.pipeline.get(123)
        self.assertEqual(123, pipeline.pipeline_id)

        self.assertEqual(
            responses.calls[0].request.url,
            DW_API + '/pipeline/123'
        )

    @responses.activate
    def test_get_pipeline_managers(self):
        """Test getting pipeline methods."""
        pipe = self.dw.pipeline.get(123)

        methods = [
            (pipe.reports.list, '/pipeline/123/report'),
            (pipe.actions.list, '/pipeline/123/action'),
            (pipe.patches.list, '/pipeline/123/patches'),
            (pipe.issuerecords.list, '/pipeline/123/issue/record'),
            (pipe.lintruns.list, '/pipeline/123/jobs/lint'),
            (pipe.mergeruns.list, '/pipeline/123/jobs/merge'),
            (pipe.buildruns.list, '/pipeline/123/jobs/build'),
            (pipe.testruns.list, '/pipeline/123/jobs/test'),
            (pipe.failures.lint.list, '/pipeline/123/jobs/lint/failures'),
            (pipe.failures.merge.list, '/pipeline/123/jobs/merge/failures'),
            (pipe.failures.build.list, '/pipeline/123/jobs/build/failures'),
            (pipe.failures.test.list, '/pipeline/123/jobs/test/failures'),
        ]

        for method, endpoint in methods:
            responses.calls.reset()
            method()
            self.assertEqual(
                responses.calls[0].request.url, DW_API + endpoint
            )

    @responses.activate
    def test_get_issue_reges(self):
        """Test issue_regex."""
        self.dw.issue_regex.list()
        self.dw.pipelines_missing_report.list()

    @responses.activate
    def test_get(self):
        """Test tests."""
        self.dw.test.list()
        test = self.dw.test.get(2)
        test.stats.get()

        self.assertListEqual(
            [DW_API + '/test', DW_API + '/test/2', DW_API + '/test/2/stats'],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_data(self):
        """Test kcidb endpoints."""
        pagination = '&last_retrieved_id=0&limit=1'
        self.dw.kcidb.data.get(name='revisions', last_retrieved_id=0, limit=1)
        self.dw.kcidb.data.get(name='builds', last_retrieved_id=0, limit=1)
        self.dw.kcidb.data.get(name='tests', last_retrieved_id=0, limit=1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/data/revisions?name=revisions' + pagination,
                DW_API + '/kcidb/data/builds?name=builds' + pagination,
                DW_API + '/kcidb/data/tests?name=tests' + pagination,
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_issues(self):
        """Test issues."""
        self.dw.issue.list()
        self.dw.issue.list(resolved=False)
        issue = self.dw.issue.get(1)
        issue.record.list()

        self.assertListEqual(
            [
                DW_API + '/issue',
                DW_API + '/issue?resolved=False',
                DW_API + '/issue/1',
                DW_API + '/issue/1/record',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_failed(self):
        """Test failed pipelines."""
        self.dw.failed_pipelines.list(stage='lint')
        self.dw.failed_pipelines.list(stage='merge')
        self.dw.failed_pipelines.list(stage='build')
        self.dw.failed_pipelines.list(stage='test')

        self.assertListEqual(
            [
                DW_API + '/pipeline/failures/lint?stage=lint',
                DW_API + '/pipeline/failures/merge?stage=merge',
                DW_API + '/pipeline/failures/build?stage=build',
                DW_API + '/pipeline/failures/test?stage=test',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_submit(self):
        """Test kcidb submit."""
        self.dw.kcidb.submit.create(data={'key': 'value'})
        self.assertEqual(
            [
                (DW_API + '/kcidb/submit', '{"data": {"key": "value"}}'),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )

    @responses.activate
    def test_kcidb(self):
        """Test kcidb."""
        self.dw.kcidb.revisions.list()
        rev = self.dw.kcidb.revisions.get(1)
        rev.builds.list()
        build = self.dw.kcidb.builds.get(1)
        build.tests.list()
        self.dw.kcidb.tests.get(1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/revisions',
                DW_API + '/kcidb/revisions/1',
                DW_API + '/kcidb/revisions/1/builds',
                DW_API + '/kcidb/builds/1',
                DW_API + '/kcidb/builds/1/tests',
                DW_API + '/kcidb/tests/1',
            ],
            [call.request.url for call in responses.calls]
        )

    @responses.activate
    def test_kcidb_issues(self):
        """Test kcidb issues."""
        rev = self.dw.kcidb.revisions.get(1)
        build = self.dw.kcidb.builds.get(1)
        test = self.dw.kcidb.tests.get(1)

        responses.calls.reset()
        rev.issues.list()
        rev.issues.get(1)
        build.issues.list()
        build.issues.get(1)
        test.issues.list()
        test.issues.get(1)

        self.assertListEqual(
            [
                DW_API + '/kcidb/revisions/1/issues',
                DW_API + '/kcidb/revisions/1/issues/1',
                DW_API + '/kcidb/builds/1/issues',
                DW_API + '/kcidb/builds/1/issues/1',
                DW_API + '/kcidb/tests/1/issues',
                DW_API + '/kcidb/tests/1/issues/1',
            ],
            [call.request.url for call in responses.calls]
        )

        responses.calls.reset()
        rev.issues.create(issue_id=2)
        build.issues.create(issue_id=2)
        test.issues.create(issue_id=2)

        self.assertEqual(
            [
                (DW_API + '/kcidb/revisions/1/issues', '{"issue_id": 2}'),
                (DW_API + '/kcidb/builds/1/issues', '{"issue_id": 2}'),
                (DW_API + '/kcidb/tests/1/issues', '{"issue_id": 2}'),
            ],
            [(call.request.url, call.request.body) for call in responses.calls]
        )
