#!/usr/bin/python3

from distutils.core import setup

setup(name='datawarehouse-api-lib',
      version='1.0',
      description='Python wrapper for Datawarehouse API.',
      packages=['datawarehouse'],
      author='CKI Team',
      license='GPLv3',
      author_email='cki-project@redhat.com',
      install_requires=open('requirements.txt').read().splitlines(),
     )
