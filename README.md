# Datawarehouse API lib

This is a Python wrapper for the Datawarehouse API.

## How to install

```shell
python3 -m pip install git+https://gitlab.com/cki-project/datawarehouse-api-lib.git
```

## How to use

This module provides all the API endpoints in an easy way.

For read only access, no token is necessary.

```python
from datawarehouse import Datawarehouse
dw = Datawarehouse('https://datawarehouse.internal.cki-project.org', token=None)
```

Besides the following examples, please check the source code for all the methods available.
All the endpoints listed on the [documentation] should be available.

### Get objects

To get a single element, for example a KCIDB Revision, you can use the `get` attribute.

```python
In [2]: dw.kcidb.revisions.get('28ad5f65c314ffdd5888d6afa61772d3032a332c')
Out[2]: KCIDBRevision(id='28ad5f65c314ffdd5888d6afa61772d3032a332c', ...)
```

### List objects

To query a list endpoint, you can use the `list` attribute.

```python
In [5]: dw.kcidb.revisions.list()
Out[5]: [KCIDBRevision(id='80fdaa2813d9290be0b88e5eed9b8436046173b8', ...),
         KCIDBRevision(id='7c04299291eb81f60fae466c06bb45836dda4f2f', ...),
         ...
]
```

### Nested objects

Some objects returned also have methods.

For example, when querying a Revision, you can access it's Builds directly.

```python
In [6]: revision = dw.kcidb.revisions.get('28ad5f65c314ffdd5888d6afa61772d3032a332c')

In [7]: revision
Out[7]: KCIDBRevision(id='28ad5f65c314ffdd5888d6afa61772d3032a332c', ...)

In [8]: revision.builds.list()
Out[8]: [KCIDBBuild(revision_id='28ad5f65c314ffdd5888d6afa61772d3032a332c', id='redhat:930137', origin='redhat', ...),
         ...]
```

### Pagination

Pagination parameters are possible using kwargs.

```python
In [10]: dw.kcidb.revisions.list(limit=1)
Out[10]: [KCIDBRevision(id='bc43d5993e02622be8b5de3c9ae48814bb6e1e1c+059bdeb9c582b35490dc8b1a8178e26c3000de65a0878fffc82107b870e80cd8', ...)]

In [11]: len(dw.kcidb.revisions.list(limit=2))
Out[11]: 2
```

[documentation]: https://cki-project.gitlab.io/datawarehouse
